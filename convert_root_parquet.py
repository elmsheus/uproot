#!/usr/bin/env python

import atlas
import uproot
import pandas as pd
import pyarrow.parquet as pq
import pyarrow as pa


def main():

  # Open DAOD file with uproot
  filename = "/Users/elmsheuser/daod_phys/user.elmsheus.18500268.EXT0._000002.DAOD_PHYS.daodphys.pool.root"
  myfile = uproot.open(filename)  
  mytree = myfile["CollectionTree"]       

  # Read 3 jet variables into pandas dataframe
  mydf = mytree.pandas.df(["AntiKt4EMTopoJetsAux.pt", "AntiKt4EMTopoJetsAux.eta", "AntiKt4EMTopoJetsAux.phi"])
  print(mydf)

  # Convert the df and save it to parquet file
  table = pa.Table.from_pandas(mydf)
  pq.write_table(table, 'jets.parquet')

  # Read back parquet file
  table2 = pq.read_table('jets.parquet')
  mydf2 = table2.to_pandas()
  print(mydf2)

  return

if __name__ == "__main__":
  # execute only if run as a script
  atlas.set_atlas()
  main()
