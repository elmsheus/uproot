#!/usr/bin/env python

#import atlas
import uproot
import pandas as pd
import pyarrow.parquet as pq
import pyarrow as pa
import awkward1 as ak

def main():

  # Open DAOD file with uproot
  filename = "DAOD_PHYSLITE.21568667._000031.pool.root.1"
  #filename = "user.elmsheus.18500268.EXT0._000002.DAOD_PHYS.daodphys.pool.root"
  #filename = "DAOD_PHYSLITE.mc.pool.root"
  myfile = uproot.open(filename)  
  mytree = myfile["CollectionTree"]  
  print("1")
  
  # Collect all variables
  allvars = mytree.keys()
  savevars = []
  skipvars = []
  nallvars = 0
  for var in allvars:
    myvar = var.decode("utf-8")
    #print(myvar)
    # Collect all Aux. variables and below
    if myvar.endswith("Aux."):
      for i in mytree[myvar].keys():
        #print (i)
        mysubvar = i.decode("utf-8")
        nallvars = nallvars + 1
        # Check if it's a python type to be able to store
        try:
          temp = mytree[i].array() 
        except ValueError:
          skipvars.append(mysubvar)
          continue
        savevars.append(mysubvar)
    # Ignore all non Aux containers
    if (myvar.endswith(".")) or (not "." in myvar):
      continue
    # Check if DynAux are python type to be able to store
    nallvars = nallvars + 1
    try:
      temp = mytree[myvar].array() 
    except ValueError:
      skipvars.append(myvar)
      continue

    if myvar in [ 'AnalysisJetsAuxDyn.NumTrkPt500',
              'AnalysisJetsAuxDyn.SumPtTrkPt500',
              'AnalysisJetsAuxDyn.NumTrkPt1000',
              'AnalysisJetsAuxDyn.TrackWidthPt1000',
              'CaloCalTopoClustersAuxDyn.e_sampl',
              'EventInfoAuxDyn.streamTagRobs',
              'EventInfoAuxDyn.streamTagDets',
    ]:
      skipvars.append(myvar)
      continue
    if any([s in myvar for s in ["definingParametersCovMatrix", "MET_Core", "e_sampl", "eta_sampl"]]):
      skipvars.append(myvar)
      continue
    
    #print(myvar)
    savevars.append(myvar)


  # Print all to be store variables
  #for var in savevars:
  #  print(var)
  print (nallvars, len(savevars))
  print("2")

  print("===========")
  print(skipvars, len(skipvars))
  print("===========")

  # Get all the to-be-writen variable branches from the TTree
  ar_dict = mytree.arrays(savevars)

  # Convert 
  ar = ak.zip({k : ak.from_awkward0(v) for k, v in ar_dict.items()}, depth_limit=1)
  print("3")

  # Write them out to parquet
  output_parquet = "daod_physlite_awkward1_awk_gzip_9.parquet"
  ak.to_parquet(ar, output_parquet, compression='gzip', compression_level=9)
  print("4")

  return

if __name__ == "__main__":
  # execute only if run as a script
  #atlas.set_atlas()
  main()
