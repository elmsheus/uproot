#!/usr/bin/env python

import atlas
import uproot
import numpy
import matplotlib.pyplot as plt


def main():
  filename = "user.elmsheus.18500268.EXT0._000002.DAOD_PHYS.daodphys.pool.root"
  events = uproot.open(filename)["CollectionTree"]
  #df = uproot.daskframe(filename, "events")

  #jets_pt = events.array("AntiKt4EMTopoJetsAux.pt")

  h1 = h2 = h3 = None
  k = 0
  num = 0
  
  #for data in events.iterate(["AntiKt4EMTopoJetsAux.pt", "AntiKt4EMTopoJetsAux.eta", "AntiKt4EMTopoJetsAux.phi", "EventInfoAux.eventNumber"], namedecode="utf-8", entrystart=0, entrystop=30000):
  for data in events.iterate(["AntiKt4EMTopoJetsAux.eta", "EventInfoAux.eventNumber"], namedecode="utf-8", entrystart=0, entrystop=30000):
    # jet pt
    #for jet in data["AntiKt4EMTopoJetsAux.pt"]:
    #  counts, edges = numpy.histogram(jet/1000., bins=200, range=(0, 200))
    #  if h1 is None:
    #    h1 = counts, edges
    #  else:
    #    h1 = h1[0] + counts, edges

    # jet phi
    #for jet in data["AntiKt4EMTopoJetsAux.phi"]:
    #  counts, edges = numpy.histogram(jet, bins=80, range=(-4, 4))
    #  if h2 is None:
    #    h2 = counts, edges
    #  else:
    #    h2 = h2[0] + counts, edges

    # jet eta
    for jet in data["AntiKt4EMTopoJetsAux.eta"]:
      counts, edges = numpy.histogram(jet, bins=200, range=(-5, 5))
      if h3 is None:
        h3 = counts, edges
      else:
        h3 = h3[0] + counts, edges

    # event number    
    for enum in data["EventInfoAux.eventNumber"]:
      num = num + 1

    k = k+1
    
  print(k)
  print(num)
  
  fig, axs = plt.subplots(2,2)

  #counts, edges = h1
  #axs[0,0].step(x=edges, y=numpy.append(counts, 0), where="post")
  #axs[0,0].set(xlabel="jet pt [GeV]", ylabel="events per bin")
             
  #counts, edges = h2
  #axs[0,1].step(x=edges, y=numpy.append(counts, 0), where="post")
  #axs[0,1].set(xlabel="jet phi", ylabel="events per bin")
  counts, edges = h3
  axs[1,0].step(x=edges, y=numpy.append(counts, 0), where="post");
  axs[1,0].set(xlabel="jet eta", ylabel="events per bin")
  
  #axs[0,0].xlim(edges[0], edges[-1]);
  #axs[0,0].ylim(0, counts.max() * 1.1);
  


  plt.savefig("jets.png")
  #plt.show()

  
  return

if __name__ == "__main__":
  # execute only if run as a script
  atlas.set_atlas()
  main()
