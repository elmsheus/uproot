#!/usr/bin/env python

import pyarrow.parquet as pq
import awkward1 as ak
import atlas
import matplotlib.pyplot as plt
import numpy as np

electron_vars = [
    "pt",
    "eta",
    "phi",
    "DFCommonElectronsLHLooseBL",
    "DFCommonElectronsLHTight",
    "topoetcone20",
    "ptvarcone20_TightTTVA_pt1000",
]

muon_vars = [
    "pt",
    "eta",
    "phi",
    "DFCommonGoodMuon",
    "topoetcone20",
    "ptvarcone30",
]

jet_vars = [
    "pt",
    "eta",
    "phi",
    "Jvt",
    # "NumTrkPt500" # ... currently vector<vector ...
]

columns = sum(
    [
        [f"{cont}.{v}" for v in vars]
        for cont, vars in [
                ("AnalysisElectronsAuxDyn", electron_vars),
                ("AnalysisJetsAuxDyn", jet_vars),
                ("AnalysisMuonsAuxDyn", muon_vars)
        ]
    ],
    []
)

#ar = ak.from_parquet("test2.parquet", columns=columns)

if __name__ == "__main__":

    atlas.set_atlas()
    import pyarrow.parquet as pq
    #from proper_xrdfile import XRDFile
    #f = XRDFile()
    #f.open("root://lcg-lrz-rootd.grid.lrz.de:1094/pnfs/lrz-muenchen.de/data/atlas/dq2/atlaslocalgroupdisk/rucio/user/nihartma/c4/40/testdata_physlite.parquet")
    f =  "daod_physlite_awkward1_awk_gzip_9.parquet"
    
    import awkward1 as ak
    ar = ak.from_parquet(f, columns=columns)

    muonpt = []
    jeteta = []
    for i in ar:
        muonpt.extend( i['AnalysisMuonsAuxDyn.pt'].tolist())
        jeteta.extend( i['AnalysisJetsAuxDyn.eta'].tolist())

    muonpt = np.array(muonpt)/1000.
    jeteta = np.array(jeteta)
        
    # Plot with matplotlib array
    fig, ax = plt.subplots(1,2, figsize=(10,5))
    ax0, ax1 = ax.flatten()
    ax0.hist(jeteta,bins=100)
    ax0.set_xlabel(r"jet $\eta$")
    ax0.set_ylabel("N jets")

    ax1.hist(muonpt,bins=100)
    ax1.set_xlabel(r"muon pt")
    ax1.set_ylabel("N muons")

    plt.show()
