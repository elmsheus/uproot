#!/usr/bin/env python

import atlas
import uproot
import pandas as pd
import pandavro as pdx

def main():

  # Open DAOD file with uproot
  filename = "user.elmsheus.18500268.EXT0._000002.DAOD_PHYS.daodphys.pool.root"
  myfile = uproot.open(filename)  
  mytree = myfile["CollectionTree"]  
  print("1")
  
  # Collect all variables
  allvars = mytree.keys()
  savevars = []
  nallvars = 0
  for var in allvars:
    myvar = var.decode("utf-8")
    #print(myvar)
    # Collect all Aux. variables and below
    if myvar.endswith("Aux."):
      for i in mytree[myvar].keys():
        mysubvar = i.decode("utf-8")
        nallvars = nallvars + 1
        # Check if it's a python type to be able to store
        try:
          temp = mytree[i].array() 
          #print(mytree[i], temp)
        except ValueError:
          continue
        if mysubvar in [ "EventInfoAux.streamTagNames", "EventInfoAux.streamTagTypes"]:
          continue
        savevars.append(mysubvar)
    # Ignore all non Aux containers
    if (myvar.endswith(".")) or (not "." in myvar):
      continue
    # Check if DynAux are python type to be able to store
    nallvars = nallvars + 1
    try:
      temp = mytree[myvar].array() 
    except ValueError:
      continue
    #print(myvar)
    savevars.append(myvar)


  # Print all to be store variables
  print("=========")
  for var in savevars:
    print(var)
  print (nallvars, len(savevars))
  print("2")
  
  # save into pandas dataframe
  mydf = mytree.pandas.df(savevars, flatten=False)
  #print(mydf)
  print("3")
  # Convert the df and save it to avro file
  pdx.to_avro('daod.avro', mydf)
  print("4")

  # Read back avro file
  saved = pdx.read_avro('jet.avro')
  print(saved)

  return

if __name__ == "__main__":
  # execute only if run as a script
  atlas.set_atlas()
  main()
