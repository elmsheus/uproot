#!/usr/bin/env python

import atlas
import uproot
import pandas as pd
import pyarrow.parquet as pq
import pyarrow as pa
import awkward1 as ak

def main():

  # Open DAOD file with uproot
  #filename = "DAOD_PHYSLITE.21568667._000031.pool.root.1"
  filename = "DAOD_PHYSLITE.mc.pool.root"
  myfile = uproot.open(filename)  
  mytree = myfile["CollectionTree"]  
  print("uproot open input: " + filename)
  
  # Collect all variables
  allvars = mytree.keys()
  savevars = []
  skipvars = []
  nallvars = 0
  for var in allvars:
    myvar = var.decode("utf-8")
    #print(myvar)
    # Collect all Aux. variables and below
    if myvar.endswith("Aux."):
      for i in mytree[myvar].keys():
        mysubvar = i.decode("utf-8")
        nallvars = nallvars + 1
        # Check if it's a python type to be able to store
        try:
          temp = mytree[i].array() 
        except ValueError:
          skipvars.append(mysubvar)
          continue
        savevars.append(mysubvar)
    # Ignore all non Aux containers
    if (myvar.endswith(".")) or (not "." in myvar):
      continue
    # Check if DynAux are python type to be able to store
    nallvars = nallvars + 1
    try:
      temp = mytree[myvar].array() 
    except ValueError:
      skipvars.append(myvar)
      continue
    #print(myvar)
    savevars.append(myvar)

  # Print all to be store variables
  #for var in savevars:
  #  print(var)
  print ("Num all variables: ", nallvars, ", Num saved variables: ", len(savevars))

  print("===========")
  print ("Skip variables: ")
  print(skipvars)
  print ("Num skip variables: ", len(skipvars))
  print("===========")
  
  # save into pandas dataframe
  mydf = mytree.pandas.df(savevars, flatten=False)
  #print(mydf)
  print("Converted to pandas.dataframe")

  # Convert the df to pyarrow table
  table = pa.Table.from_pandas(mydf)
  print("Converted to pyarrow.table")

  # Convert arrow table to awkward 
  ak_array = ak.from_arrow(table)
  print("Converted to awkward1")

  # Write out awkard to parquet
  output_parquet = 'daod_physlite_data15_awk_gzip_9.parquet'
  ak.to_parquet(ak_array, output_parquet, compression='gzip', compression_level=9)
  print("Written to ", output_parquet)

  # Read back parquet file
  #table2 = pq.read_table('jets.parquet')
  #mydf2 = table2.to_pandas()
  #print(mydf2)

  return

if __name__ == "__main__":
  # execute only if run as a script
  atlas.set_atlas()
  main()
