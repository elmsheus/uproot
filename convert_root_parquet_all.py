#!/usr/bin/env python

import atlas
import uproot
import pandas as pd
import pyarrow.parquet as pq
import pyarrow as pa


def main():

  # Open DAOD file with uproot
  filename = "user.elmsheus.18500268.EXT0._000002.DAOD_PHYS.daodphys.pool.root"
  #filename = "DAOD_PHYSLITE.mc.pool.root"
  myfile = uproot.open(filename)  
  mytree = myfile["CollectionTree"]  
  print("1")
  
  # Collect all variables
  allvars = mytree.keys()
  savevars = []
  skipvars = []
  nallvars = 0
  for var in allvars:
    myvar = var.decode("utf-8")
    print(myvar)
    # Collect all Aux. variables and below
    if myvar.endswith("Aux."):
      for i in mytree[myvar].keys():
        mysubvar = i.decode("utf-8")
        nallvars = nallvars + 1
        # Check if it's a python type to be able to store
        try:
          temp = mytree[i].array() 
        except ValueError:
          skipvars.append(mysubvar)
          continue
        savevars.append(mysubvar)
    # Ignore all non Aux containers
    if (myvar.endswith(".")) or (not "." in myvar):
      continue
    # Check if DynAux are python type to be able to store
    nallvars = nallvars + 1
    try:
      temp = mytree[myvar].array() 
    except ValueError:
      skipvars.append(myvar)
      continue
    print(myvar)
    savevars.append(myvar)


  # Print all to be store variables
  #for var in savevars:
  #  print(var)
  print (nallvars, len(savevars))
  print("2")

  print("===========")
  print(skipvars, len(skipvars))
  print("===========")
  
  # save into pandas dataframe
  mydf = mytree.pandas.df(savevars, flatten=False)
  #print(mydf)
  print("3")
  # Convert the df and save it to parquet file
  table = pa.Table.from_pandas(mydf)
  print("4")
  #pq.write_table(table, 'daod_physlite_mc_lz4_9.parquet', compression='lz4', compression_level=9)
  pq.write_table(table, 'daod_phys_zstd_22.parquet', compression='zstd', compression_level=22)
  print("5")
  # Read back parquet file
  #table2 = pq.read_table('jets.parquet')
  #mydf2 = table2.to_pandas()
  #print(mydf2)

  return

if __name__ == "__main__":
  # execute only if run as a script
  atlas.set_atlas()
  main()
