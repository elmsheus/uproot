#!/usr/bin/env python

import sys
import ROOT
#import faulthandler

skipvars = ['EventInfoAux.xAOD::AuxInfoBase', 'EventInfoAux.detDescrTags', 'xTrigDecisionAux.xAOD::AuxInfoBase', 'METAssoc_AntiKt4EMPFlowAux.xAOD::AuxContainerBase', 'METAssoc_AntiKt4EMPFlowAux.jetLink', 'METAssoc_AntiKt4EMPFlowAux.objectLinks', 'METAssoc_AntiKt4EMPFlowAux.calkey', 'METAssoc_AntiKt4EMPFlowAux.trkkey', 'METAssoc_AntiKt4EMPFlowAux.overlapIndices', 'METAssoc_AntiKt4EMPFlowAux.overlapTypes', 'METAssoc_AntiKt4EMTopoAux.xAOD::AuxContainerBase', 'METAssoc_AntiKt4EMTopoAux.jetLink', 'METAssoc_AntiKt4EMTopoAux.objectLinks', 'METAssoc_AntiKt4EMTopoAux.calkey', 'METAssoc_AntiKt4EMTopoAux.trkkey', 'METAssoc_AntiKt4EMTopoAux.overlapIndices', 'METAssoc_AntiKt4EMTopoAux.overlapTypes', 'METAssoc_AntiKt4LCTopoAux.xAOD::AuxContainerBase', 'METAssoc_AntiKt4LCTopoAux.jetLink', 'METAssoc_AntiKt4LCTopoAux.objectLinks', 'METAssoc_AntiKt4LCTopoAux.calkey', 'METAssoc_AntiKt4LCTopoAux.trkkey', 'METAssoc_AntiKt4LCTopoAux.overlapIndices', 'METAssoc_AntiKt4LCTopoAux.overlapTypes', 'Kt4EMPFlowEventShapeAux.xAOD::AuxInfoBase', 'Kt4EMTopoOriginEventShapeAux.xAOD::AuxInfoBase', 'Kt4LCTopoOriginEventShapeAux.xAOD::AuxInfoBase', 'AntiKt10LCTopoJetsAux.xAOD::AuxContainerBase', 'AntiKt10LCTopoJetsAux.constituentLinks', 'AntiKt10LCTopoTrimmedPtFrac5SmallR20JetsAux.xAOD::AuxContainerBase', 'AntiKt10LCTopoTrimmedPtFrac5SmallR20JetsAux.constituentLinks', 'AntiKt10TruthTrimmedPtFrac5SmallR20JetsAux.xAOD::AuxContainerBase', 'AntiKt10TruthTrimmedPtFrac5SmallR20JetsAux.constituentLinks', 'AntiKt2PV0TrackJetsAux.xAOD::AuxContainerBase', 'AntiKt2PV0TrackJetsAux.constituentLinks', 'AntiKt4EMPFlowJetsAux.xAOD::AuxContainerBase', 'AntiKt4EMPFlowJetsAux.constituentLinks', 'AntiKt4EMTopoJetsAux.xAOD::AuxContainerBase', 'AntiKt4EMTopoJetsAux.constituentLinks', 'AntiKt4LCTopoJetsAux.xAOD::AuxContainerBase', 'AntiKt4LCTopoJetsAux.constituentLinks', 'AntiKt4TruthJetsAux.xAOD::AuxContainerBase', 'AntiKt4TruthJetsAux.constituentLinks', 'AntiKt4TruthWZJetsAux.xAOD::AuxContainerBase', 'AntiKt4TruthWZJetsAux.constituentLinks', 'AntiKt10LCTopoJetsAuxDyn.GhostAntiKt2TrackJet', 'AntiKt10LCTopoTrimmedPtFrac5SmallR20JetsAuxDyn.GhostAntiKt2TrackJet', 'AntiKt2PV0TrackJetsAuxDyn.ConeExclBHadronsFinal', 'AntiKt2PV0TrackJetsAuxDyn.ConeExclCHadronsFinal', 'AntiKt4EMPFlowJetsAuxDyn.GhostTrack', 'AntiKt4EMTopoJetsAuxDyn.ConeExclBHadronsFinal', 'AntiKt4EMTopoJetsAuxDyn.ConeExclCHadronsFinal', 'AntiKt4EMTopoJetsAuxDyn.GhostTrack', 'AntiKt4LCTopoJetsAuxDyn.GhostTrack', 'AntiKt4TruthWZJetsAuxDyn.GhostBHadronsFinal', 'CombinedMuonTrackParticlesAuxDyn.alignEffectChId', 'ElectronsAuxDyn.trackParticleLinks', 'ElectronsAuxDyn.caloClusterLinks', 'EventInfoAuxDyn.streamTagRobs', 'EventInfoAuxDyn.streamTagDets', 'ExtrapolatedMuonTrackParticlesAuxDyn.alignEffectChId', 'MuonsAuxDyn.muonSegmentLinks', 'PhotonsAuxDyn.caloClusterLinks', 'PhotonsAuxDyn.vertexLinks', 'PrimaryVerticesAuxDyn.trackParticleLinks', 'PrimaryVerticesAuxDyn.neutralParticleLinks', 'TauJetsAuxDyn.hadronicPFOLinks', 'TauJetsAuxDyn.shotPFOLinks', 'TauJetsAuxDyn.chargedPFOLinks', 'TauJetsAuxDyn.neutralPFOLinks', 'TauJetsAuxDyn.pi0PFOLinks', 'TauJetsAuxDyn.protoChargedPFOLinks', 'TauJetsAuxDyn.protoNeutralPFOLinks', 'TauJetsAuxDyn.protoPi0PFOLinks', 'TauJetsAuxDyn.tauTrackLinks', 'TauTracksAuxDyn.trackLinks', 'TrigMatch_HLT_e28_lhtight_nod0_ivarlooseAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIMAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e32_lhtight_nod0_ivarlooseAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e60_lhmedium_nod0_L1EM24VHIAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e60_lhmedium_nod0_L1EM24VHIMAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e80_lhmedium_nod0_L1EM24VHIAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e80_lhmedium_nod0_L1EM24VHIMAuxDyn.TrigMatchedObjects', 'TruthBosonAuxDyn.parentLinks', 'TruthBosonAuxDyn.childLinks', 'TruthElectronsAuxDyn.parentLinks', 'TruthElectronsAuxDyn.childLinks', 'TruthNeutrinosAuxDyn.parentLinks', 'TruthNeutrinosAuxDyn.childLinks', 'TruthPhotonsAuxDyn.parentLinks', 'TruthPhotonsAuxDyn.childLinks', 'TruthTopAuxDyn.parentLinks', 'TruthTopAuxDyn.childLinks', 'egammaClustersAuxDyn.constituentClusterLinks', 'GSFConversionVerticesAuxDyn.trackParticleLinks', 'GSFConversionVerticesAuxDyn.neutralParticleLinks', 'TruthMuonsAuxDyn.parentLinks', 'TruthMuonsAuxDyn.childLinks', 'TruthTausAuxDyn.parentLinks', 'TruthTausAuxDyn.childLinks', 'TrigMatch_HLT_e28_lhtight_nod0_L1EM24VHIM_e15_etcut_L1EM7_ZeeAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e28_lhtight_nod0_e15_etcut_L1EM7_ZeeAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2mu14AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2mu15_L12MU10AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_3mu6AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_3mu6_msonlyAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_3mu8_msonlyAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu26_ivarmediumAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu28_ivarmediumAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu60AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu60_0eta105_msonlyAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2e17_lhvloose_nod0_L12EM15VHIAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2e19_lhvloose_nod0AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2e24_lhvloose_nod0AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu80AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e140_lhloose_nod0_L1EM24VHIAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e140_lhloose_nod0_L1EM24VHIMAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e24_lhloose_nod0_2e12_lhloose_nod0_L1EM20VH_3EM10VHAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VHAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2g25_tight_L12EM20VHAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_g35_medium_g25_medium_L12EM20VHAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_g140_tightAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_g140_tight_L1EM24VHIMAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu20_2mu4noL1AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu22_2mu4noL1AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu22_mu8noL1_mu6noL1AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu24_2mu4noL1AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu24_mu10noL1AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu24_mu8noL1AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu26_mu10noL1AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu26_mu8noL1AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_mu28_mu8noL1AuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2g20_tight_icalovloose_L12EM15VHIAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2g22_tight_L12EM15VHIAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2g22_tight_icalovloose_L12EM15VHIAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_g200_looseAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_g200_loose_L1EM24VHIMAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2g50_loose_L12EM20VHAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2g60_loose_L12EM20VHAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e300_etcutAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_e300_etcut_L1EM24VHIMAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_g300_etcut_L1EM24VHIAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_g300_etcut_L1EM24VHIMAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2g20_tight_icalotight_L12EM15VHIAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2g22_tight_icalotight_L12EM15VHIAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2g25_loose_g15_looseAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_2g25_loose_g20_looseAuxDyn.TrigMatchedObjects', 'TrigMatch_HLT_3g25_looseAuxDyn.TrigMatchedObjects']

def main(argv):

  #ROOT.gInterpreter.ProcessLine("#include <vector>")
  #ROOT.gInterpreter.GenerateDictionary("vector<vector >;vector<vector<vector > >", "vector")
  
  # Open DAOD file with uproot
  #filename = "user.elmsheus.18500268.EXT0._000002.DAOD_PHYS.daodphys.pool.root"
  #filename ="/afs/cern.ch/user/e/elmsheus/public/daod_phys/mc16/user.elmsheus.18500268.EXT0._000002.DAOD_PHYS.daodphys.pool.root"
  filename ="DAOD_PHYSLITE.mc.pool.root"
  
  oldfile = ROOT.TFile.Open(filename)
  oldtree = oldfile.Get("CollectionTree")

  # Deactivate all branches
  oldtree.SetBranchStatus( "*", 0 )
  
  # Decactivate skipvars
  listOfBranches = oldtree.GetListOfBranches()
  for branch in listOfBranches:
    branchname = branch.GetName()
    if ("Aux" in branchname) and (not "Link" in branchname):
    #if ("Aux" in branchname):
      print(branchname)
      oldtree.SetBranchStatus( branchname, 1 )
  for i in skipvars:
    oldtree.SetBranchStatus( i , 0 )

      
  #oldtree.SetBranchStatus( "Analysis*" , 1 )
  #faulthandler.enable()
  
  newfile = ROOT.TFile("daodphys_zlib_1.root", "recreate")
  # https://root.cern.ch/doc/master/Compression_8h_source.html
  #newfile.SetCompressionSettings(106)
  newfile.SetCompressionSettings(101)
  #newfile.SetCompressionSettings(209)
  #newtree = oldtree.CloneTree(-1,"fast")
  newtree = oldtree.CloneTree()
  newtree.Print()
  #oldtree.Print()
  newfile.Write()

  newfile.Close()
  oldfile.Close()

  return

if __name__ == "__main__":
  #ROOT.gROOT.LoadMacro("./AtlasStyle.C")
  #ROOT.SetAtlasStyle()
  main(sys.argv[1:])
