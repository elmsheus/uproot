#!/usr/bin/env python

import atlas
import h5py
import zarr
import numpy as np
from sys import stdout

def main():

  source = h5py.File('daod_physlite_zlib_38.h5', mode='r')
  dest = zarr.open_group('daod_physlite.zarr', mode='w')
  zarr.copy_all(source, dest, log=stdout)

  dest.tree()  
  source.close()

  return

if __name__ == "__main__":
  # execute only if run as a script
  atlas.set_atlas()
  main()
