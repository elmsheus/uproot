#!/usr/bin/env python

import atlas
import uproot
import pandas as pd
import pandavro as pdx

def main():

  # Open DAOD file with uproot
  filename = "user.elmsheus.18500268.EXT0._000002.DAOD_PHYS.daodphys.pool.root"
  myfile = uproot.open(filename)  
  mytree = myfile["CollectionTree"]       

  # Read 3 jet variables into pandas dataframe
  mydf = mytree.pandas.df(["AntiKt4EMTopoJetsAux.pt", "AntiKt4EMTopoJetsAux.eta", "AntiKt4EMTopoJetsAux.phi"])
  print(mydf)

  # Convert the df and save it to avro file
  pdx.to_avro('jet.avro', mydf)

  # Read back avro file
  saved = pdx.read_avro('jet.avro')
  print(saved)

  return

if __name__ == "__main__":
  # execute only if run as a script
  atlas.set_atlas()
  main()
