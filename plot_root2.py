#!/usr/bin/bin python

import sys
import ROOT

def main(argv):

  filename = "user.elmsheus.18500268.EXT0._000002.DAOD_PHYS.daodphys.pool.root"
  tfile = ROOT.TFile.Open(filename)
  tree = tfile.CollectionTree

  # Disable all branches but the AntiKt4EMTopoJetsAux ones:
  tree.SetBranchStatus( "*", 0 )
  tree.SetBranchStatus( "AntiKt4EMTopoJetsAux*", 1 )
  tree.SetBranchStatus( "EventInfoAux.eventNumber", 1 )

  h1 = ROOT.TH1F("h1","h1", 100, -5., 5.)
  h2 = ROOT.TH1F("h2","h2", 100, 0., 100000.)

  
  c1 = ROOT.TCanvas( 'c1', 'c1', 200, 10, 700, 500 )
  c1.Divide(2,1)
  h1.GetXaxis().SetTitle( 'jet #eta' )
  h1.GetYaxis().SetTitle( 'N jets' )

  for entryNum in range(0,tree.GetEntries()):
    tree.GetEntry(entryNum)
    jetetas = getattr(tree,"AntiKt4EMTopoJetsAux.eta")
    for jeteta in jetetas:
      h1.Fill(jeteta)
      
  c1.cd(1)
  h1.Draw()
  c1.cd(2)
  h2.Draw()
  c1.Update()
  c1.Print("jets_root.pdf")

  #raw_input("Press Enter to continue...")
  
  return

if __name__ == "__main__":
  ROOT.gROOT.LoadMacro("./AtlasStyle.C")
  ROOT.SetAtlasStyle()
  main(sys.argv[1:])




