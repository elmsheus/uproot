#!/usr/bin/env python

import atlas
import uproot
import pandas as pd
import pyarrow.parquet as pq
import pyarrow as pa


def main():

  # Open DAOD file with uproot
  #filename = "user.elmsheus.18500268.EXT0._000002.DAOD_PHYS.daodphys.pool.root"
  filename = "DAOD_PHYSLITE.mc.pool.root"
  myfile = uproot.open(filename)  
  mytree = myfile["CollectionTree"]  
  print("1")
  
  # Collect all variables
  allvars = mytree.keys()
  savevars = []
  skipvars = []
  nallvars = 0
  for var in allvars:
    myvar = var.decode("utf-8")
    print(myvar)
    # Collect all Aux. variables and below
    if myvar.endswith("Aux."):
      for i in mytree[myvar].keys():
        mysubvar = i.decode("utf-8")
        nallvars = nallvars + 1
        # Check if it's a python type to be able to store
        try:
          temp = mytree[i].array() 
        except ValueError:
          skipvars.append(mysubvar)
          continue
        savevars.append(mysubvar)
    # Ignore all non Aux containers
    if (myvar.endswith(".")) or (not "." in myvar):
      continue
    # Check if DynAux are python type to be able to store
    nallvars = nallvars + 1
    try:
      temp = mytree[myvar].array() 
    except ValueError:
      skipvars.append(myvar)
      continue
    print(myvar)
    savevars.append(myvar)


  # Print all to be store variables
  #for var in savevars:
  #  print(var)
  print (nallvars, len(savevars))
  print("2")

  print("===========")
  print(skipvars, len(skipvars))
  print("===========")
  
  # save into pandas dataframe
  mydf = mytree.pandas.df(savevars, flatten=False)
  #print(mydf)
  print("3")
  # save df to hdf5 file
  # file compression currently broken in pandas
  mydf.to_hdf("daod_physlite_zlib_38.h5", key="mydf", mode="w",
              complevel=5, complib='zlib',
              encoding='utf-8')

  #with pd.HDFStore("daod_physlite_zlib_5.h5", "w", complevel=5, complib="zlib") as store:
  #  store.put("mydf", mydf)
    
  # Read back h5 file
  #print(mydf2)

  return

if __name__ == "__main__":
  # execute only if run as a script
  atlas.set_atlas()
  main()
