#!/usr/bin/env python

import atlas
import pandas as pd
import pyarrow.parquet as pq
import matplotlib.pyplot as plt
import numpy as np

# allow TrueType fonts for pdf using conda
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42
#plt.rcParams['text.usetex'] = True

def main():

  # Read only certain branches of parquet file
  table2 = pq.read_table('daod_gzip.parquet', columns=["AntiKt4EMTopoJetsAux.eta", "EventInfoAux.eventNumber"])
  
  # Read full parquet file
  #table2 = pq.read_table('daod.parquet')

  # Convert to pandas.dataframe
  mydf2 = table2.to_pandas()
  #print(mydf2["AntiKt4EMTopoJetsAux.eta"])
  print(mydf2.keys())

  # Get all variable values into array (use in case of flat jet.parquet)
  #jeteta=mydf2["AntiKt4EMTopoJetsAux.eta"].values.flatten()

  # Need to flatten array in case of full DAOD
  jeteta=np.concatenate(mydf2["AntiKt4EMTopoJetsAux.eta"].values.flatten()).ravel()
  # Run numbers are not an array
  evtnum=mydf2["EventInfoAux.eventNumber"].values.flatten()
  
  # Plot with pandas dataframe
  #hist = mydf2.hist(column="AntiKt4EMTopoJetsAux.eta",bins=100)

  # Plot with matplotlib array
  fig, ax = plt.subplots(1,2, figsize=(10,5))
  ax0, ax1 = ax.flatten()
  ax0.hist(jeteta,bins=100)
  ax0.set_xlabel(r"jet $\eta$")
  ax0.set_ylabel("N jets")

  ax1.hist(evtnum,bins=100)
  ax1.set_xlabel(r"run number")
  ax1.set_ylabel("N events")

  # Single plot histogram
  #plt.hist(jeteta,bins=100)
  #plt.xlabel(r"jet $\eta$")
  #plt.ylabel("N jets")

  plt.savefig("jets_parquet.pdf")
  # keep canvas open
  #plt.show()
  
  return

if __name__ == "__main__":
  # execute only if run as a script
  atlas.set_atlas()
  main()
